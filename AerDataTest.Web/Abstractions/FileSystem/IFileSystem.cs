﻿using AerDataTest.Web.Abstractions.FileSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AerDataTest.Web.Abstractions
{
    public interface IFileSystem
    {
        IFileInfo GetFileInfo(string filePath);
        string[] GetDirectories(string path);
        bool DirectoryExist(string path);
        string[] GetFilesRecoursive(string folderPath);
    }
}
