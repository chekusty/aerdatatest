﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AerDataTest.Web.Abstractions.FileSystem;

namespace AerDataTest.Web.Abstractions
{
    public class SystemIOWrapper : IFileSystem
    {
        public bool DirectoryExist(string path)
        {
            return Directory.Exists(path);
        }

        public string[] GetDirectories(string path)
        {
            return Directory.GetDirectories(path);
        }

        public IFileInfo GetFileInfo(string filePath)
        {
            return new FileInfoWrapper(filePath);
        }

        public string[] GetFilesRecoursive(string directoryPath)
        {
            return Directory.GetFiles(directoryPath, "*.*", SearchOption.AllDirectories);
        }
    }
}
