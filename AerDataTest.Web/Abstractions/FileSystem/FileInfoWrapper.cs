﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AerDataTest.Web.Abstractions.FileSystem
{
    public class FileInfoWrapper : IFileInfo
    {
        private FileInfo _fileInfo;
        public FileInfoWrapper(string filePath)
        {
            _fileInfo = new FileInfo(filePath);
        }

        public long Length { get => _fileInfo.Length; }
    }
}
