﻿
namespace AerDataTest.Web.Abstractions.FileSystem
{
    public interface IFileInfo
    {
        long Length { get; }
    }
}
