﻿namespace AerDataTest.Web.Model
{
    public class DirectoryData
    {
        public string Name { get; set; }
        public long Size { get; set; }
    }
}
