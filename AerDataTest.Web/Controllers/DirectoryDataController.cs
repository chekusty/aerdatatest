﻿using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AerDataTest.Services;
using AerDataTest.Web.Model;
using AerDataTest.Web;

namespace AerDataTest.Controllers
{
    [Route("api/[controller]")]
    public class DirectoryDataController: Controller
    {
        private IFileSystemService _fileSystemService;

        public DirectoryDataController(IFileSystemService fileSystemService)
        {
            _fileSystemService = fileSystemService;
        }

        //https://localhost:44398/api/DirectoryData?path=C%3A%5CUsers%5CUser%5CDesktop
        [HttpGet]
        [ProducesResponseType(typeof(IList<DirectoryData>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Get(string path, int numberOfResults = 5)
        {
            if (string.IsNullOrWhiteSpace(path)) return BadRequest(Constants.PathCannotBeEmptyMessage);
            if (!_fileSystemService.DirectoryExists(path)) return BadRequest(Constants.DirectoryDoesNotExistMessage);
            if (numberOfResults <= 0) return BadRequest(Constants.NumberOfResultsMustBeGreaterThenZeroMessage);

            var result = _fileSystemService.GetBiggestDirectories(path, numberOfResults);

            return Ok(result);
        }
    }
}
