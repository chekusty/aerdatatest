﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AerDataTest.Web
{
    public class Constants
    {
        public const string PathCannotBeEmptyMessage = "The directory path cannot be empty.";
        public const string DirectoryDoesNotExistMessage = "Provided directory does not exist.";
        public const string NumberOfResultsMustBeGreaterThenZeroMessage = "The number of results must be greater then 0.";
    }
}
