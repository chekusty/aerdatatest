import { Component, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-directory-data',
  templateUrl: './directory-data.component.html',
  styleUrls: ['./directory-data.component.css']
})
export class DirectoryDataComponent {
  public directoryData: DirectoryInfo[];
  public path: string;
  public directoryDataLoading: boolean;
  public message: string;

  private http: HttpClient;
  private basePath: string;

  constructor(httpClient: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.http = httpClient;
    this.basePath = baseUrl;
  }

  public GetDirectoryData()
  {
    this.directoryData = null;
    this.message = null;

    if(!this.path || !this.path.trim())
    {
      this.message = "Path cannot be empty";
      return;
    }

    this.directoryDataLoading = true;

    let params = new HttpParams().set('path', this.path);
    this.http.get<DirectoryInfo[]>(this.basePath + 'api/DirectoryData', { params: params }).subscribe(result => {
      this.directoryData = result;
      this.directoryDataLoading = false;
    }, error => {
      console.error(error);
      if(error.status == 400)
      {
        this.message = error.message;
      }
      this.directoryData = null;
      this.directoryDataLoading = false;
    });
  }
}

interface DirectoryInfo {
  name: string;
  size: number;
}

