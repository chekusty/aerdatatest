﻿using AerDataTest.Web.Abstractions;
using AerDataTest.Web.Model;
using System.Collections.Generic;
using System.Linq;

namespace AerDataTest.Services
{
    public class FileSystemService : IFileSystemService
    {
        private readonly IFileSystem _fileSystem;
        private readonly object _syncRoot = new object();

        public FileSystemService(IFileSystem fileSystem)
        {
            _fileSystem = fileSystem;
        }

        public IList<DirectoryData> GetBiggestDirectories(string path, int numberOfDirectories)
        {
            var allDirectoriesUnderPath = _fileSystem.GetDirectories(path);
            var result = new List<DirectoryData>();
            
            allDirectoriesUnderPath.AsParallel().WithDegreeOfParallelism(5).ForAll(directoryPath =>
            {
                var directoryData = GetDirectoryData(directoryPath);
                lock (_syncRoot)
                {
                    result.Add(directoryData);
                }
            });

            return result.OrderByDescending(x => x.Size).Take(numberOfDirectories).ToList();
        }

        private DirectoryData GetDirectoryData(string path)
        {
            var directoryInfo = new DirectoryData();
            directoryInfo.Name = path;

            foreach (string name in _fileSystem.GetFilesRecoursive(path))
            {
                var info = _fileSystem.GetFileInfo(name);
                directoryInfo.Size += info.Length;
            }

            return directoryInfo;
        }

        public bool DirectoryExists(string path)
        {
            return _fileSystem.DirectoryExist(path);
        }
    }
}
