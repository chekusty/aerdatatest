﻿using AerDataTest.Web.Model;
using System.Collections.Generic;

namespace AerDataTest.Services
{
    public interface IFileSystemService
    {
        IList<DirectoryData> GetBiggestDirectories(string path, int numberOfDirectories);

        bool DirectoryExists(string path);
    }
}
