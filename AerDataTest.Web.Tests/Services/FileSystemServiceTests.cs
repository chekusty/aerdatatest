using AerDataTest.Services;
using AerDataTest.Web.Abstractions;
using AerDataTest.Web.Abstractions.FileSystem;
using FluentAssertions;
using NSubstitute;
using System.Linq;
using Xunit;

namespace AerDataTest.Web.Tests
{
    public class FileSystemServiceTests
    {
        IFileSystem _fileSystemMock;

        public FileSystemServiceTests()
        {
            _fileSystemMock = Substitute.For<IFileSystem>();
            _fileSystemMock.GetDirectories("root").Returns(new string[] { "dir1", "dir2" , "dir3" , "dir4" , "dir5" , "dir6" , "dir7" });
            _fileSystemMock.GetFilesRecoursive("dir1").Returns(new string[] { "file1_1" });
            _fileSystemMock.GetFilesRecoursive("dir2").Returns(new string[] { "file2_1", "file2_2" }); ;
            _fileSystemMock.GetFilesRecoursive("dir3").Returns(new string[] { "file3_1", "file3_2" , "file3_3" }); ;
            _fileSystemMock.GetFilesRecoursive("dir4").Returns(new string[] { "file4_1" }); ;
            _fileSystemMock.GetFilesRecoursive("dir5").Returns(new string[] { "file5_1" }); ;
            _fileSystemMock.GetFilesRecoursive("dir6").Returns(new string[] { "file6_1" }); ;
            _fileSystemMock.GetFilesRecoursive("dir7").Returns(new string[] { "file7_1" }); ;

            var file1_1 = Substitute.For<IFileInfo>();
            var file2_1 = Substitute.For<IFileInfo>();
            var file2_2 = Substitute.For<IFileInfo>();
            var file3_1 = Substitute.For<IFileInfo>();
            var file3_2 = Substitute.For<IFileInfo>();
            var file3_3 = Substitute.For<IFileInfo>();
            var file4_1 = Substitute.For<IFileInfo>();
            var file5_1 = Substitute.For<IFileInfo>();
            var file6_1 = Substitute.For<IFileInfo>();
            var file7_1 = Substitute.For<IFileInfo>();

            file1_1.Length.Returns(10);
            file2_1.Length.Returns(20);
            file2_2.Length.Returns(21);
            file3_1.Length.Returns(30);
            file3_2.Length.Returns(31);
            file3_3.Length.Returns(32);
            file4_1.Length.Returns(40);
            file5_1.Length.Returns(50);
            file6_1.Length.Returns(60);
            file7_1.Length.Returns(70);

            _fileSystemMock.GetFileInfo("file1_1").Returns(file1_1);
            _fileSystemMock.GetFileInfo("file2_1").Returns(file2_1);
            _fileSystemMock.GetFileInfo("file2_2").Returns(file2_2);
            _fileSystemMock.GetFileInfo("file3_1").Returns(file3_1);
            _fileSystemMock.GetFileInfo("file3_2").Returns(file3_2);
            _fileSystemMock.GetFileInfo("file3_3").Returns(file3_3);
            _fileSystemMock.GetFileInfo("file4_1").Returns(file4_1);
            _fileSystemMock.GetFileInfo("file5_1").Returns(file5_1);
            _fileSystemMock.GetFileInfo("file6_1").Returns(file6_1);
            _fileSystemMock.GetFileInfo("file7_1").Returns(file7_1);
        }

        [Theory]
        [InlineData(5, 5)]
        [InlineData(3, 3)]
        [InlineData(10, 7)]
        public void GetBiggestDirectories_WhenCalled_ReturnsRequestedNumberOfRecords(int requestedNumber, int expectedNumber)
        {
            //Arrange
            var fileSystemService = new FileSystemService(_fileSystemMock);

            //Act
            var result = fileSystemService.GetBiggestDirectories("root", requestedNumber);

            //Assert
            result.Should().HaveCount(expectedNumber);
        }

        [Fact]
        public void GetBiggestDirectories_WhenCalled_ReturnsResultsOrderedByDirectorySize()
        {
            //Arrange
            var fileSystemService = new FileSystemService(_fileSystemMock);

            //Act
            var result = fileSystemService.GetBiggestDirectories("root", 7).ToList();

            //Assert
            Assert.Equal("dir3", result[0].Name);
            Assert.Equal("dir7", result[1].Name);
            Assert.Equal("dir6", result[2].Name);
            Assert.Equal("dir5", result[3].Name);
            Assert.Equal("dir2", result[4].Name);
            Assert.Equal("dir4", result[5].Name);
            Assert.Equal("dir1", result[6].Name);
        }

        [Fact]
        public void GetBiggestDirectories_WhenCalled_ReturnsCorrectlyCalculatedResults()
        {
            //Arrange
            var fileSystemService = new FileSystemService(_fileSystemMock);

            //Act
            var result = fileSystemService.GetBiggestDirectories("root", 7).ToList();

            //Assert
            Assert.Equal(93, result[0].Size);
            Assert.Equal(70, result[1].Size);
            Assert.Equal(60, result[2].Size);
            Assert.Equal(50, result[3].Size);
            Assert.Equal(41, result[4].Size);
            Assert.Equal(40, result[5].Size);
            Assert.Equal(10, result[6].Size);
        }
    }
}
