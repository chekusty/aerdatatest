using AerDataTest.Controllers;
using AerDataTest.Services;
using AerDataTest.Web.Abstractions;
using AerDataTest.Web.Abstractions.FileSystem;
using AerDataTest.Web.Model;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace AerDataTest.Web.Tests
{
    public class DirectoryDataControllerTests
    {
        IFileSystemService _fileSystemServiceMock;

        public DirectoryDataControllerTests()
        {
            _fileSystemServiceMock = Substitute.For<IFileSystemService>();
            _fileSystemServiceMock.DirectoryExists("path").Returns(true);
            _fileSystemServiceMock.GetBiggestDirectories(Arg.Is<string>(x => x == "path"), Arg.Is<int>(x=> x > 0)).Returns(new List<DirectoryData>());
        }

        [Theory]
        [InlineData("", 1 , Constants.PathCannotBeEmptyMessage)]
        [InlineData(" ", 1, Constants.PathCannotBeEmptyMessage)]
        [InlineData(null, 1, Constants.PathCannotBeEmptyMessage)]
        [InlineData("path", 0, Constants.NumberOfResultsMustBeGreaterThenZeroMessage)]
        [InlineData("path", -1, Constants.NumberOfResultsMustBeGreaterThenZeroMessage)]
        [InlineData("invalidpath", 3, Constants.DirectoryDoesNotExistMessage)]

        public void Get_WhenCalledWithWrongParameters_ReturnBadRequest(string path, int numberOfResults, string expectedMessage)
        {
            //Arrange
            var controller  = new DirectoryDataController(_fileSystemServiceMock);

            //Act
            var result = controller.Get(path, numberOfResults) as ObjectResult;

            //Assert
            Assert.True(result != null, "Result has unexpected type");
            Assert.Equal(StatusCodes.Status400BadRequest, result.StatusCode);
            Assert.Equal(expectedMessage, result.Value);
        }
    }
}
