##AerData TEST

Implement a small web application which should show 5 biggest folders from a specified path.
Requirements:

* The solution should consist of a Web page and Web API
* The page should show a list of 5 folders ordered by size from biggest to smaller
* It should be implemented in .Net core
* The UI should be implemented as SPA. Preferably in latest Angular
* Web API endpoint should be implemented using async/await pattern
* Include some tests

---

## Structure

The backbone of the project was created using **dotnet new angular**

Solution consists of 2 projects **Asp.Net MVC** and **XUnit Tests**

User interface is implemented as Angular based SPA. Hosted together with WebAPI

Backend  - .Net Core 2.2
Frontend - Angular 6

---

## How to work with the project

1. Visual Studio. Open **AerDataTest.Web.sln** in the Visual Studio and start debugging
2. Dotnet CLI. run **dotnet run** from **AerDataTest.Web** folder